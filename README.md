## Домашнее задание ##
### Работа с индексами, join'ами, статистикой ###
Цель: - знать и уметь применять основные виды индексов PostgreSQL
- строить и анализировать план выполнения запроса
- уметь оптимизировать запросы для с использованием индексов
- знать и уметь применять различные виды join'ов
- строить и анализировать план выполенения запроса
- оптимизировать запрос
- уметь собирать и анализировать статистику для таблицы
1 вариант:
Создать индексы на БД, которые ускорят доступ к данным.
В данном задании тренируются навыки:
- определения узких мест
- написания запросов для создания индекса
- оптимизации
### Необходимо: ###
**1) Создать индекс к какой-либо из таблиц вашей БД**

Решил использовать базу данных Taxi  которую использовал по лекции BidData

### 2) Прислать текстом результат команды explain,в которой используется данный индекс ###

**postgres=# explain**

**SELECT payment_type**

**FROM taxi_trips**

**group by payment_type;**

     
                                                      QUERY PLAN
-----------------------------------------------------------------------------------------------------------------------

      
      Group  (cost=1000.46..377152.10 rows=10 width=8)
       Group Key: payment_type
        ->     Gather Merge  (cost=1000.46..377152.05 rows=20 width=8)
             Workers Planned: 2
         ->     Group  (cost=0.44..376149.72 rows=10 width=8)
                  Group Key: payment_type
                  ->  Parallel Index Only Scan using idx_taxi on taxi_trips  (cost=0.44..347444.96 rows=11481903 width=8)
       JIT:
       Functions: 4
       Options: Inlining false, Optimization false, Expressions true, Deforming true
     

(10 rows)

Без индекса запрос выполнялся Time: 435683.685 ms (07:15.684)

После того как создал индекс запрос делается за  Time: 2969.381 ms (00:02.969)

Существенно ускорился запрос 

Посмотрел статистику по индексам 

postgres=# select *
from pg_stat_user_indexes
where relname='taxi_trips';

      relid | indexrelid | schemaname |  relname   | indexrelname | idx_scan | idx_tup_read | idx_tup_fetch
      -------+------------+------------+------------+--------------+----------+--------------+---------------
      16402 |      16416 | public     | taxi_trips | idx_taxi     |        3 |     27556568 |           451
     (1 row)

**3) Реализовать индекс для полнотекстового поиска**

Создал индекс типа gin предназначен для работы с текстом

postgres=# CREATE INDEX idx_text2 ON taxi_trips USING gin(payment_type);

CREATE INDEX

 Сделела SELECT по колонке payment_type по определенному имени 

Time: 394051.076 ms (06:34.051)



             postgres=# explain  SELECT * FROM taxi_trips WHERE payment_type  @@ 'Way2ride';

                                          QUERY PLAN
-----------------------------------------------------------------------------------------------
              Gather  (cost=3855.82..535718.37 rows=137783 width=421)
               Workers Planned: 2
               ->  Parallel Bitmap Heap Scan on taxi_trips  (cost=2855.82..520940.07 rows=57410 width=421)
                   Recheck Cond: (payment_type @@ '''Way2ride'''::tsquery)
                    ->  Bitmap Index Scan on idx_text2  (cost=0.00..2821.37 rows=137783 width=0)
                       Index Cond: (payment_type @@ '''Way2ride'''::tsquery)
                 JIT:
                 Functions: 2
                 Options: Inlining true, Optimization true, Expressions true, Deforming true


без индекса данный запрос отрабатывал  очень долго а с индексом за секнды ))

**4) Реализовать индекс на часть таблицы или индекс на поле с функцией**

Создал обычный индекс на часть таблице до номера 100-ой строки

create index idx_tips_100 on taxi_trips(tips) where tips < 100;

Сделал проверку роботает ли индекс до 100

                         explain select * from taxi_trips
                         where tips = 99;
                                       QUERY PLAN
----------------------------------------------------------------------------------------
                    Index Scan using idx_tips_100 on taxi_trips  (cost=0.56..10494.67 rows=3009 width=421)
                    Index Cond: (tips = '99'::numeric)


Проверил работает ли после 100

                  explain select * from taxi_trips
                  where tips = 100;

                                     QUERY PLAN
------------------------------------------------------------------------------------
                  Gather  (cost=1000.00..1650108.69 rows=3009 width=421)
                  Workers Planned: 2
                  ->  Parallel Seq Scan on taxi_trips  (cost=0.00..1648807.79 rows=1254 width=421)
                     Filter: (tips = '100'::numeric)



**5) Создать индекс на несколько полей**

Создал обычный индекс на два поля 

CREATE INDEX idx_2 ON taxi_trips (tips, trip_total);

Сделал select по первой колонке и по второй

explain select tips, trip_total

from taxi_trips

where tips=100 and trip_total<108;

                                     QUERY PLAN
              -------------------------------------------------------------------------------------
              Index Only Scan using idx_2 on taxi_trips  (cost=0.56..11192.51 rows=3000 width=10)
                   Index Cond: ((tips = '100'::numeric) AND (trip_total < '108'::numeric))
              (2 rows)

**6) Написать комментарии к каждому из индексов** 

прокоментировал при создании каждого из инексов 

**7) Описать что и как делали и с какими проблемами столкнулись**

**При реализации полнотекстового поиска необходимо было сталбец по которому будет запущен поиск перевести в необходимый формат 
ALTER TABLE taxi_trips ALTER COLUMN payment_type TYPE tsvector USING payment_type::tsvector; 

2 вариант:
В результате выполнения ДЗ вы научитесь пользоваться
различными вариантами соединения таблиц.
В данном задании тренируются навыки:
- написания запросов с различными типами соединений
Необходимо:

**1) Реализовать прямое соединение двух или более таблиц**
 
            postgres=# select * from test2;
            id |    name    |       model       |  made
            ----+------------+-------------------+---------
             1 | 'Toyota'   | Land Cruser Prado | Japan
             2 | 'Lada'     | Vesta             | Russia
             3 | 'Mercedes' | Gelenvagen        | Germany
            (3 rows)


               postgres=# select * from zapchasti;
               id |   name   |       model       |    zapchact
               ----+----------+-------------------+----------------
                1 | Toyota   | Land Cruser Prado | bamper
                2 | Lada     | Vesta             | fara
                3 | Mercedes | Gelenvagen        | lobovoe steklo
                4 | Mercedes | Gelenvagen        | fara
               (4 rows)


               postgres=# select *
               from test2 t
               join zapchasti z
               on t.model = z.model;
                id |    name    |       model       |  made   | id |   name   |       model       |    zapchact
               ----+------------+-------------------+---------+----+----------+-------------------+----------------
               1 | 'Toyota'   | Land Cruser Prado | Japan   |  1 | Toyota   | Land Cruser Prado | bamper
               2 | 'Lada'     | Vesta             | Russia  |  2 | Lada     | Vesta             | fara
               3 | 'Mercedes' | Gelenvagen        | Germany |  3 | Mercedes | Gelenvagen        | lobovoe steklo
               3 | 'Mercedes' | Gelenvagen        | Germany |  4 | Mercedes | Gelenvagen        | fara
               (4 rows)

Сделал прямое соединение двух таблиц по колонкам name

**2) Реализовать левостороннее (или правостороннее) соединение двух или более таблиц**

               postgres=# select *
               from test2 t
               left join zapchasti z
               on t.model = z.model;
               id |    name    |       model       |  made   | id |   name   |       model       |    zapchact
               ----+------------+-------------------+---------+----+----------+-------------------+----------------
                1 | 'Toyota'   | Land Cruser Prado | Japan   |  1 | Toyota   | Land Cruser Prado | bamper
                2 | 'Lada'     | Vesta             | Russia  |  2 | Lada     | Vesta             | fara
                3 | 'Mercedes' | Gelenvagen        | Germany |  3 | Mercedes | Gelenvagen        | lobovoe steklo
                3 | 'Mercedes' | Gelenvagen        | Germany |  4 | Mercedes | Gelenvagen        | fara
               (4 rows)

Сделал левостороннее соединение двух таблиц по колонкам model


3) **Реализовать кросс соединение двух или более таблиц**

                   postgres=# select *
                   from test2 t
                   cross join zapchasti z;
                   id |    name    |       model       |  made   | id |   name   |       model       |    zapchact
                  ----+------------+-------------------+---------+----+----------+-------------------+----------------
                    1 | 'Toyota'   | Land Cruser Prado | Japan   |  1 | Toyota   | Land Cruser Prado | bamper
                    2 | 'Lada'     | Vesta             | Russia  |  1 | Toyota   | Land Cruser Prado | bamper
                    3 | 'Mercedes' | Gelenvagen        | Germany |  1 | Toyota   | Land Cruser Prado | bamper
                    1 | 'Toyota'   | Land Cruser Prado | Japan   |  2 | Lada     | Vesta             | fara
                    2 | 'Lada'     | Vesta             | Russia  |  2 | Lada     | Vesta             | fara
                    3 | 'Mercedes' | Gelenvagen        | Germany |  2 | Lada     | Vesta             | fara
                    1 | 'Toyota'   | Land Cruser Prado | Japan   |  3 | Mercedes | Gelenvagen        | lobovoe steklo
                    2 | 'Lada'     | Vesta             | Russia  |  3 | Mercedes | Gelenvagen        | lobovoe steklo
                    3 | 'Mercedes' | Gelenvagen        | Germany |  3 | Mercedes | Gelenvagen        | lobovoe steklo
                    1 | 'Toyota'   | Land Cruser Prado | Japan   |  4 | Mercedes | Gelenvagen        | fara
                    2 | 'Lada'     | Vesta             | Russia  |  4 | Mercedes | Gelenvagen        | fara
                    3 | 'Mercedes' | Gelenvagen        | Germany |  4 | Mercedes | Gelenvagen        | fara
(12 rows)
Сделал кросс соединение двух таблиц 
данные из первой таблицы соединились построчно с данными из второй таблицы

**4) Реализовать полное соединение двух или более таблиц**

               postgres=# select *
               from test2 t
               full join zapchasti z
               on t.name = z.name;
               id |    name    |       model       |  made   | id |   name   |       model       |    zapchact
               ----+------------+-------------------+---------+----+----------+-------------------+----------------
                  |            |                   |         |  1 | Toyota   | Land Cruser Prado | bamper
                  |            |                   |         |  2 | Lada     | Vesta             | fara
                  |            |                   |         |  3 | Mercedes | Gelenvagen        | lobovoe steklo
                  |            |                   |         |  4 | Mercedes | Gelenvagen        | fara
                1 | 'Toyota'   | Land Cruser Prado | Japan   |    |          |                   |
                2 | 'Lada'     | Vesta             | Russia  |    |          |                   |
                3 | 'Mercedes' | Gelenvagen        | Germany |    |          |                   |
               (7 rows)

 Сделал полное соединение двух таблиц по колонке name, соответствия не были найдены, по этому колонки из
противоположной таблицы пусты

**5) Реализовать запрос, в котором будут использованы разные типы соединений**

               postgres=# select *
               from test2 t
               full join zapchasti z
               on t.name = z.name
               
               union all

               select *
               from test2 t
               cross join zapchasti z;
               id |    name    |       model       |  made   | id |   name   |       model       |    zapchact
               ----+------------+-------------------+---------+----+----------+-------------------+----------------
                   |            |                   |         |  1 | Toyota   | Land Cruser Prado | bamper
                   |            |                   |         |  2 | Lada     | Vesta             | fara
                   |            |                   |         |  3 | Mercedes | Gelenvagen        | lobovoe steklo
                   |            |                   |         |  4 | Mercedes | Gelenvagen        | fara
                 1 | 'Toyota'   | Land Cruser Prado | Japan   |    |          |                   |
                 2 | 'Lada'     | Vesta             | Russia  |    |          |                   |
                 3 | 'Mercedes' | Gelenvagen        | Germany |    |          |                   |
                 1 | 'Toyota'   | Land Cruser Prado | Japan   |  1 | Toyota   | Land Cruser Prado | bamper
                 2 | 'Lada'     | Vesta             | Russia  |  1 | Toyota   | Land Cruser Prado | bamper
                 3 | 'Mercedes' | Gelenvagen        | Germany |  1 | Toyota   | Land Cruser Prado | bamper
                 1 | 'Toyota'   | Land Cruser Prado | Japan   |  2 | Lada     | Vesta             | fara
                 2 | 'Lada'     | Vesta             | Russia  |  2 | Lada     | Vesta             | fara
                 3 | 'Mercedes' | Gelenvagen        | Germany |  2 | Lada     | Vesta             | fara
                 1 | 'Toyota'   | Land Cruser Prado | Japan   |  3 | Mercedes | Gelenvagen        | lobovoe steklo
                 2 | 'Lada'     | Vesta             | Russia  |  3 | Mercedes | Gelenvagen        | lobovoe steklo
                 3 | 'Mercedes' | Gelenvagen        | Germany |  3 | Mercedes | Gelenvagen        | lobovoe steklo
                 1 | 'Toyota'   | Land Cruser Prado | Japan   |  4 | Mercedes | Gelenvagen        | fara
                 2 | 'Lada'     | Vesta             | Russia  |  4 | Mercedes | Gelenvagen        | fara
                 3 | 'Mercedes' | Gelenvagen        | Germany |  4 | Mercedes | Gelenvagen        | fara
                (19 rows)
В данном запросе  используется два типа соединения full и cross объединяет их команда union all

6) Сделать комментарии на каждый запрос
7) К работе приложить структуру таблиц, для которых
выполнялись соединения
